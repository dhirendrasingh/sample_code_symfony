<?php

namespace SmsWorkFlowBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Psr\Log\LoggerInterface;

/**
 * Create helper function for whole applications
 * @author SH
 *
 */
class CommonServices 
{
    /*
     * defining the properties.
     */

    protected $em;
    
    private $container;
    
    private $logger;
    
    const TEST_SCHEDULE_LIMIT = 15;

    // We need to inject this variables later.
    public function __construct(
        EntityManager $entityManager, 
        Container $container, 
        LoggerInterface $logger
    ) {
        $this->em = $entityManager;
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * get All MTTypes
     * @return array
     */
    public function getAllMTType() {
        $allMtTypes = $this->em
                ->getRepository('SmsWorkFlowBundle:MTType')
                ->findBy(array('status' => 1));
        return $allMtTypes;
    }

    /**
     * Get all active testflows
     * @return active TestFlow Object
     */
    public function activeTestFlows() {
        $activeTestFlows = $this->em
                ->getRepository('SmsWorkFlowBundle:TestFlow')
                ->findBy(array('status' => 1));
        return $activeTestFlows;
    }

    /**
     * Get all active devices
     * @return active device Object
     */
    public function activeDevices() {
        $query = $this->em->createQueryBuilder('m')
                        ->select('m')
                        ->from('SmsWorkFlowBundle\Entity\DeviceManagment', 'm')
                        ->where('m.status = 1')
                        ->orderBy('m.deviceName', 'ASC')->getQuery()->getResult();
        return $query;
    }

    /**
     * Get all schedule tests
     * @return array
     */
    public function getAllTestSchedule() {
        $page = $this->container->get('request')->query->get('page') ? $this->container->get('request')->query->get('page') : 1;
        $limit = self::TEST_SCHEDULE_LIMIT;
        $start_from = ($page - 1) * $limit;
        $testSchedule = $this->em
                ->getRepository('SmsWorkFlowBundle:TestSchedule')
                ->findBy([], ['id' => 'DESC'], $limit, $start_from);
        return $testSchedule;
    }

    /**
     * Get individual test schedule
     * @param int $id
     * @return object
     */
    public function getSpecificTestSchedule($id) {
        $specificTestSchedule = $this->em
                ->getRepository('SmsWorkFlowBundle:TestSchedule')
                ->find($id);
        return $specificTestSchedule;
    }

    /**
     * Get individual testFlow
     * @param int $id
     * @return array
     */
    public function getTestFlowById($id) {
        $results = $this->em->createQueryBuilder('m')
                ->select('m')
                ->from('SmsWorkFlowBundle:TestFlow', 'm')
                ->where('m.id = :identifier')
                ->setParameter('identifier', $id)
                ->getQuery()
                ->getResult();
        $tests = [];
        foreach ($results as $result) {
            $tests = $result;
        }

        return $tests;
    }

    /**
     * get device name by their Id
     * @param type $id
     * @return array
     */
    public function getDeviceById($id) {
        $results = $this->em->createQueryBuilder('m')
                ->select('m')
                ->from('SmsWorkFlowBundle:DeviceManagment', 'm')
                ->where('m.id = :identifier')
                ->setParameter('identifier', $id)
                ->getQuery()
                ->getResult();
        $devices = [];
        foreach ($results as $result) {
            $devices = $result;
        }
        return $devices;
    }

    /**
     * Get label of schedule test
     * @param int $status
     * @return string
     */
    public function testScheduleStatus($status) {
        $state = '';
        switch ($status) {
            case 1:
                $state = 'Running';
                break;
            case 2:
                $state = 'Completed';
                break;
            case 3:
                $state = 'Pending';
                break;
            case 4:
                $state = 'Failed';
                break;
            default:
                $state = 'Pending';
        }
        return $state;
    }

}
