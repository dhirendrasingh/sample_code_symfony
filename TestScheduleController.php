<?php

namespace SmsWorkFlowBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use SmsWorkFlowBundle\Entity\TestSchedule;

/**
 * Define the Test Schedule class for manage schedule for SMS
 * @author SH
 *
 */
class TestScheduleController extends Controller
{

    /**
     * Create new Test Schedule for SMS flow
     * @param Request $request
     * @return RedirectResponse
     */
    public function createTestScheduleAction(Request $request)
    {
        $test_chedule = new TestSchedule();
        $em = $this->getDoctrine()->getManager();
        $post = Request::createFromGlobals();

        $created_by = $post->request->get('created_by');
        $test_flow = $post->request->get('testflow');
        $device_managment = $post->request->get('device');
        $schedule_type = $post->request->get('schedule_type');
        $schedule_at = $post->request->get('schedule_at') ? $post->request->get('schedule_at') : '';
        $recurring_time = $post->request->get('recurring_time');
        $end_at = $post->request->get('end_at') ? $post->request->get('end_at') : '';
        $recurring = $post->request->get('recurring');
        $status = $post->request->get('status');
        if ($recurring) {
            if ($end_at < $schedule_at) {
                $this->addFlash('error', 'Schedule date can not be grater than end date.');
                return $this->redirectToRoute('easyadmin', array(
                            'action' => 'new',
                            'entity' => 'TestSchedule'
                ));
            }
        }

        if ($schedule_type == 2 && $recurring) {
            $this->recurringTestSchedule($schedule_type, $recurring, $recurring_time, $end_at, $schedule_at, $created_by, $test_flow, $device_managment, $status, $em);
        }

        if ($schedule_type == 2 && !$recurring) {
            $this->createScheduleTest($schedule_type, $schedule_at, $created_by, $test_flow, $device_managment, $status, $em);
        }

        if ($schedule_type == 1) {
            $this->createInstantTest($test_chedule, $test_flow, $device_managment, $schedule_type, $recurring, $recurring_time, $end_at, $created_by, $status, $schedule_at, $em);
        }
        // redirect to the 'list' view of the given entity
        return $this->redirectToRoute('easyadmin', array(
                    'action' => 'list',
                    'entity' => 'TestSchedule'
        ));
    }

    /**
     * Get Recurring test Schedule params
     * @param int $schedule_type
     * @param int $recurring
     * @param datetime $recurring_time
     * @param datetime $end_at
     * @param datetime $schedule_at
     * @param int $created_by
     * @param string $test_flow
     * @param string $device_managment
     * @param int $status
     * @param object $em
     */
    public function recurringTestSchedule($schedule_type, $recurring, $recurring_time, $end_at, $schedule_at, $created_by, $test_flow, $device_managment, $status, $em)
    {
        $interval = abs(strtotime($end_at) - strtotime($schedule_at));
        $minutes = round($interval / 60);
        $recurringTimes = array('1' => 15, '2' => 30, '3' => 60, '4' => 10080);
        $time = isset($recurringTimes[$recurring_time]) ? $recurringTimes[$recurring_time] : 15;
        $slots = ($minutes / $time);
        $this->createRecurringTestSchedule($schedule_type, $recurring, $recurring_time, $end_at, $schedule_at, $created_by, $test_flow, $device_managment, $status, $em, $slots, $time);
    }

    /**
     * Create Recurring Test Schedule
     * @param int $schedule_type
     * @param int $recurring
     * @param datetime $recurring_time
     * @param datetime $end_at
     * @param datetime $schedule_at
     * @param int $created_by
     * @param string $test_flow
     * @param string $device_managment
     * @param int $status
     * @param type $em
     * @param type $slots
     * @param object $time
     */
    public function createRecurringTestSchedule($schedule_type, $recurring, $recurring_time, $end_at, $schedule_at, $created_by, $test_flow, $device_managment, $status, $em, $slots, $time)
    {
        for ($i = 1; $i <= floor($slots); $i++) {
            $schedule_at_now = new \DateTime($schedule_at);
            $schedule_at_now->add(new \DateInterval('PT' . ($time * $i) . 'M'));

            $test_chedule = new TestSchedule();
            $test_chedule->setTestflow($test_flow);
            $test_chedule->setDevice($device_managment);
            $test_chedule->setScheduleType($schedule_type);
            $test_chedule->setRecurring($recurring);
            $test_chedule->setRecurringTime($recurring_time);
            $test_chedule->setEndAt(null);
            $test_chedule->setCreatedBy($created_by);
            $test_chedule->setStatus($status);
            $test_chedule->setFailed(\FALSE);
            $test_chedule->setScheduleAt($schedule_at_now);
            $em->persist($test_chedule);
            $em->flush();
        }
    }

    /**
     * Create Instant Test
     * @param int $test_chedule
     * @param string $test_flow
     * @param string $device_managment
     * @param int $schedule_type
     * @param int $recurring
     * @param datetime $recurring_time
     * @param datetime $end_at
     * @param int $created_by
     * @param int $status
     * @param datetime $schedule_at
     * @param object $em
     */
    public function createInstantTest($test_chedule, $test_flow, $device_managment, $schedule_type, $recurring, $recurring_time, $end_at, $created_by, $status, $schedule_at, $em)
    {
        $testScheduleService = $this->container->get('app.common_function_generator');
        $tets_flow_obj = $testScheduleService->getTestFlowById($test_flow);
        $test_flow_name = $tets_flow_obj->getName();
        $test_chedule->setTestflow($test_flow);
        $test_chedule->setDevice($device_managment);
        $test_chedule->setScheduleType($schedule_type);
        $test_chedule->setRecurring($recurring);
        $test_chedule->setRecurringTime($recurring_time);
        $test_chedule->setEndAt(null);
        $test_chedule->setCreatedBy($created_by);
        $test_chedule->setStatus($status);
        $test_chedule->setFailed(\FALSE);
        $test_chedule->setScheduleAt(new \DateTime($schedule_at));
        $em->persist($test_chedule);
        $em->flush();
        if ($test_chedule->getId()) {
            $msg = ['body' => $this->container->getParameter('NOTIFICATION_BODY'),
                'title' => $test_flow_name, //$this->container->getParameter('NOTIFICATION_TITLE'),
                'content_available' => true,
                'priority' => 1,
                "schedule_id" => $test_chedule->getId() //$test_chedule->getId()
            ];
            $testScheduleService->sentPushNotification($msg);
        }
    }

    /**
     * Modify create test schedule
     * @param Request $request
     * @return RedirectResponse
     */
    public function editTestScheduleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $test_id = $request->request->get('test_schedule');
        $test_schedule = $this->getDoctrine()->getRepository('SmsWorkFlowBundle:TestSchedule')->find($test_id);

        $test_flow = $request->request->get('testflow');
        $device = $request->request->get('device');

        $test_schedule->setTestflow($test_flow);
        $test_schedule->setDevice($device);
        $test_schedule->setScheduleType($request->request->get('schedule_type'));
        $test_schedule->setScheduleAt(new \DateTime($request->request->get('schedule_at')));
        $em->persist($test_schedule);
        $em->flush();

        return $this->redirectToRoute('easyadmin', array(
                    'action' => 'list',
                    'entity' => 'TestSchedule',
        ));
    }

    /**
     *
     * @param int $schedule_type
     * @param datetime $schedule_at
     * @param int $created_by
     * @param string $test_flow
     * @param string $device_managment
     * @param int $status
     * @param object $em
     */
    public function createScheduleTest($schedule_type, $schedule_at, $created_by, $test_flow, $device_managment, $status, $em)
    {
        $test_chedule = new TestSchedule();
        $test_chedule->setTestflow($test_flow);
        $test_chedule->setDevice($device_managment);
        $test_chedule->setScheduleType($schedule_type);
        $test_chedule->setCreatedBy($created_by);
        $test_chedule->setStatus($status);
        $test_chedule->setFailed(\FALSE);
        $test_chedule->setScheduleAt(new \DateTime($schedule_at));
        $em->persist($test_chedule);
        $em->flush();
    }

    /**
     * Search Test Schedule
     * @param Request $request
     * @return RedirectResponse
     */
    public function serachTestScheduleAction(Request $request)
    {
        $post = Request::createFromGlobals();
        if ($post->request->has('action')) {
            $query = $post->request->get('query');
            $testScheduleService = $this->container->get('app.common_function_generator');
            $serachResults = $testScheduleService->testScheduleSearchResult($query);
            return $this->render('easy_admin/TestSchedule/search.html.twig', array(
                        'results' => $serachResults,
                        'totalResult' => count($serachResults)
            ));
        }
        return $this->redirectToRoute('easyadmin', array(
                    'action' => 'list',
                    'entity' => 'TestSchedule',
        ));
    }
}
